﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using CoinbasePro.Shared.Types;
using CryptoTracker.Common.Models;

namespace CryptoTracker.Common.ViewModels
{
	public class CryptoFiatPairModel : INotifyPropertyChanged
	{
		// Fields
		private string _price;
		private string _volume;
		private string _dailyChange;
		private readonly string quoteCurrency;
		private string quoteSymbol;
		private ObservableCollection<OrderModel> orders;
		private readonly SynchronizationContext _synchronizationContext;

		// Events
		public event PropertyChangedEventHandler PropertyChanged;

		public ObservableCollection<OrderModel> Orders
		{
			get => orders;
			set
			{
				orders = value;
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( nameof( Orders ) ) );
			}
		}


		// Properties
		public string Price
		{
			get => _price;
			private set
			{
				_price = value;
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( nameof( Price ) ) );
			}
		}

		public string Volume
		{
			get => _volume;
			private set
			{
				_volume = value;
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( nameof( Volume ) ) );
			}
		}

		public string DailyChange
		{
			get => _dailyChange;
			private set
			{
				_dailyChange = value;
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( nameof( DailyChange ) ) );
			}
		}

		public string Name { get; set; }
		public ProductType Id { get; private set; }


		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="id"></param>
		/// <param name="baseCurrency"></param>
		/// <param name="quoteCurrency"></param>
		public CryptoFiatPairModel( ProductType id, string baseCurrency, string quoteCurrency, SynchronizationContext synchronizationContext )
		{
			Orders = new ObservableCollection<OrderModel>();
			Id = id;
			this.quoteCurrency = quoteCurrency;
			ConvertQuoteStringToSymbol();
			Name = $"{baseCurrency}-{quoteCurrency}";
			_synchronizationContext = synchronizationContext;
		}

		/// <summary>
		/// Add a new order
		/// </summary>
		/// <param name="order"></param>
		/// <param name="volume"></param>
		/// <param name="dailyChange"></param>
		public void AddOrder( OrderModel order, string volume, decimal dailyChange )
		{
			if ( order == null ) return;

			_synchronizationContext.Send( x => Orders.Insert( 0, order ), null );

			Price = !quoteSymbol.Equals( "NA" ) ? $"{quoteSymbol}{order.Price:#,0.#######}" : $"{order.Price:#,0.#######} {quoteCurrency}";

			Volume = volume;
			DailyChange = $"{dailyChange:n2}%";
		}

		public ObservableCollection<OrderModel> GetOrders()
		{
			return Orders;
		}

		/// <summary>
		/// Convert the quote string to a currency symbol
		/// </summary>
		/// <returns></returns>
		private bool ConvertQuoteStringToSymbol()
		{
			switch ( quoteCurrency )
			{
				case "GBP":
					quoteSymbol = "£";
					return true;

				case "USD":
					quoteSymbol = "$";
					return true;

				case "EUR":
					quoteSymbol = "€";
					return true;
				default:
					quoteSymbol = "NA";
					return false;
			}
		}
	}
}

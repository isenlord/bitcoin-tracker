﻿using System;
using System.Threading;
using CoinbasePro.Shared.Utilities.Extensions;
using CoinbasePro.WebSocket.Models.Response;
using CryptoTracker.Common.Models;
using SuperSocket.ClientEngine;

namespace CryptoTracker.Common.ViewModels
{
	public class CoinbaseViewModel
	{

		// Global variable
		public static readonly CoinbaseModel Coinbase = new CoinbaseModel();

		// Fields
		private readonly SynchronizationContext _synchronizationContext;

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="synchronizationContext"></param>
		public CoinbaseViewModel( SynchronizationContext synchronizationContext )
		{
			_synchronizationContext = synchronizationContext;
			Initialise();

			Coinbase.SubscriptionsChanged += HandleSubscriptions;
		}


		/// <summary>
		/// Initialise the coinbase connector
		/// </summary>
		public void Initialise()
		{
			if (Coinbase.Client == null) return;
			Coinbase.Client.WebSocket.OnErrorReceived += WebSocket_OnErrorReceived;
			Coinbase.Client.WebSocket.OnWebSocketError += WebSocket_OnWebSocketError;
			Coinbase.Client.WebSocket.OnTickerReceived += WebSocket_OnTickerReceived;
		}

		/// <summary>
		/// Display the ticker data
		/// </summary>
		/// <param name="ticker"></param>
		private void DisplayData( Ticker ticker )
		{
			// Create the currency pairing strings
			var type = ticker.ProductId;
			var baseCurrency = type.BaseCurrency().ToString();
			var quoteCurrency = type.QuoteCurrency().ToString();
			var lastSize = ticker.LastSize;


			// Get price, volume and size stats
			var currentPrice = ticker.Price;
			var open24HPrice = ticker.Open24H;
			var dailyVolume = $"{ticker.Volume24H:n0} {baseCurrency}";
			var change24H = ( ( currentPrice - open24HPrice ) / open24HPrice ) * 100;

			// Create the order book entry string
			var time = ticker.Time.ToString( "HH:mm:ss" );

			// Use a CryptoFiatPairing to create the form items and display the fed in data
			var cfPair = Coinbase.GetCryptoFiatPair( type );

			if ( cfPair is null )
			{
				// Does not already exist, so create and add to list
				cfPair = new CryptoFiatPairModel( type, baseCurrency, quoteCurrency, _synchronizationContext );
				_synchronizationContext.Send( x => Coinbase.AddPair( cfPair ), null );
			}

			// Add the order to the CryptroFiatPairing
			Coinbase.AddToOrderBook( cfPair, time, lastSize, currentPrice, dailyVolume, change24H );
		}

		/// <summary>
		/// Handle new product subscriptions
		/// </summary>
		private void HandleSubscriptions( object sender, EventArgs e )
		{
			if ( Coinbase.Client?.WebSocket.State == WebSocket4Net.WebSocketState.Open )
			{
				// Set up the client again - this avoid a subscription error being thrown
				Coinbase.Client?.WebSocket.Stop();
				//Coinbase.Client = null;
				//Initialise();
			}
			// If ProductTypes is populated, start subscribing
			if ( Coinbase.SubscriptionCount != 0 )
			{
				Coinbase.SubscribeToFeed();
			}
		}

		/// <summary>
		/// Handle an OnTickerReceived event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void WebSocket_OnTickerReceived( object sender, WebfeedEventArgs<Ticker> e )
		{
			DisplayData( e.LastOrder );
		}

		/// <summary>
		/// Handle an OnWebSocketError event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void WebSocket_OnWebSocketError( object sender, WebfeedEventArgs<ErrorEventArgs> e )
		{
			//MessageBox.Show( e.LastOrder.Exception.Message );
		}

		/// <summary>
		/// Handle an OnErrorReceived event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void WebSocket_OnErrorReceived( object sender, WebfeedEventArgs<Error> e )
		{
			// No channels provided error doesn't seem to be a genuine error
			if ( e.LastOrder.Reason != "No channels provided" )
			{
				//MessageBox.Show( $"{e.LastOrder.Message} - {e.LastOrder.Reason}" );
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using CoinbasePro;
using CoinbasePro.Network.Authentication;
using CoinbasePro.Services.Products.Models;
using CoinbasePro.Shared.Types;
using CoinbasePro.WebSocket.Types;
using CryptoTracker.Common.ViewModels;

namespace CryptoTracker.Common.Models
{
	public delegate void SubscriptionAddedDelegate( object sender, EventArgs args );

	public class CoinbaseModel : INotifyPropertyChanged
	{
		/// <summary>
		/// Delegate for when a crypto-fiat pair is added
		/// </summary>
		public event SubscriptionAddedDelegate SubscriptionsChanged;

		/// <summary>
		/// Property changed delegate
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		// Fields
		private readonly List<ChannelType> channels;
		private readonly ObservableCollection<CryptoFiatPairModel> cryptoFiatPairs;
		private readonly List<ProductType> subscribedProducts;
		private readonly Authenticator authenticator;
		private ObservableCollection<Product> products;

		// Properties
		public CoinbaseProClient Client { get; set; }

		public ObservableCollection<Product> Products
		{
			get => products;
			set
			{
				products = value;
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( nameof( Products ) ) );
			}
		}

		public int SubscriptionCount => subscribedProducts.Count;

		/// <summary>
		/// Default constructor
		/// </summary>
		public CoinbaseModel()
		{
			// Initialise variables
			authenticator = new Authenticator( "3bc6c91ecf66435fde8ead64aae10727", "ezLAa72BnSWO/ZCCOt7gy5qMRoLongg5uKnEfVc+wPfqNolGoUV/ubhWW3P3w/U7+UQ6Tgy6IPGI94EJ3ekncA==", "vzzku2q9xcc" );

			InitialiseClient();

			cryptoFiatPairs = new ObservableCollection<CryptoFiatPairModel>();
			subscribedProducts = new List<ProductType>();

			RetrieveProducts();

			// When not providing any channels, the socket will subscribe to all channels
			channels = new List<ChannelType>() { ChannelType.Ticker };

		}

		/// <summary>
		/// Initialise the client
		/// </summary>
		public void InitialiseClient()
		{
			Client = new CoinbaseProClient( authenticator );
		}

		/// <summary>
		/// Add a new crypto-fiat pair and invoke the delegate
		/// </summary>
		/// <param name="cryptoFiatPair"></param>
		public void AddPair( CryptoFiatPairModel cryptoFiatPair )
		{
			if ( cryptoFiatPair != null )
			{
				cryptoFiatPairs.Add( cryptoFiatPair );
			}
		}

		/// <summary>
		/// Add a new product subscription
		/// </summary>
		/// <param name="productType"></param>
		public void AddSubscription( ProductType productType )
		{
			if ( subscribedProducts.Contains( productType ) ) return;
			subscribedProducts.Add( productType );
			SubscriptionsChanged?.Invoke( this, new EventArgs() { } );
		}

		/// <summary>
		/// Remove the subscription and associated crytoFiatPairModel
		/// </summary>
		/// <param name="productType"></param>
		public void RemoveSubscription( ProductType productType )
		{
			if ( !subscribedProducts.Contains( productType ) ) return;
			subscribedProducts.Remove( productType );
			SubscriptionsChanged?.Invoke( this, new EventArgs() { } );

			var cryptoFiatPair = GetCryptoFiatPair( productType );
			if ( cryptoFiatPair != null )
			{
				cryptoFiatPairs.Remove( cryptoFiatPair );
			}
		}

		/// <summary>
		/// Return the crypto-fiat pair matching the passed in ID.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>
		/// The crypto-fiat pair or null if not found.
		/// </returns>
		public CryptoFiatPairModel GetPair( ProductType id ) => cryptoFiatPairs.FirstOrDefault( x => x.Id == id );

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ObservableCollection<CryptoFiatPairModel> GetPairs()
		{
			return cryptoFiatPairs;
		}

		/// <summary>
		/// Subscribe to all the product feeds in the products list
		/// </summary>
		public void SubscribeToFeed()
		{
			Client.WebSocket.Start( subscribedProducts, channels );

		}

		/// <summary>
		/// Retrieve cryptoFiatPair or return null
		/// </summary>
		/// <param name="productType"></param>
		/// <returns></returns>
		public CryptoFiatPairModel GetCryptoFiatPair( ProductType productType )
		{
			// Look for the existence of the CryptoFiatPairing based on the passed in string
			CryptoFiatPairModel cryptoFiatPair = GetPair( productType );

			return cryptoFiatPair;
		}

		/// <summary>
		/// Add the order to the cryptoFiatPair
		/// </summary>
		/// <param name="cryptoFiatPair"></param>
		/// <param name="time"></param>
		/// <param name="size"></param>
		/// <param name="price"></param>
		/// <param name="volume"></param>
		/// <param name="dailyChange"></param>
		public void AddToOrderBook( CryptoFiatPairModel cryptoFiatPair, string time, decimal size, decimal price, string volume, decimal dailyChange )
		{
			var order = new OrderModel( price, size, time );
			cryptoFiatPair.AddOrder( order, volume, dailyChange );
		}

		/// <summary>
		/// Get the list of products
		/// </summary>
		private async void RetrieveProducts()
		{
			var response = await Client.ProductsService.GetAllProductsAsync();
			var responseList = response.ToList();
			responseList.Sort( ( x, y ) => x.DisplayName.CompareTo( y.DisplayName ) );
			Products = new ObservableCollection<Product>( responseList );
		}
	}
}

﻿using System.ComponentModel;

namespace CryptoTracker.Common.Models
{
	public class OrderModel : INotifyPropertyChanged
	{
		// Fields
		private string _time;
		private decimal _price;
		private decimal _size;

		// Properties
		public string Time
		{
			get => _time;
			set
			{
				_time = value;
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( nameof( Time ) ) );
			}
		}

		public decimal Price
		{
			get => _price;
			set
			{
				_price = value;
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( nameof( Price ) ) );
			}
		}

		public decimal Size
		{
			get => _size;
			set
			{
				_size = value;
				PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( nameof( Size ) ) );
			}
		}

		// Events
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="price"></param>
		/// <param name="size"></param>
		/// <param name="time"></param>
		public OrderModel( decimal price, decimal size, string time )
		{
			Price = price;
			Size = size;
			Time = time;
		}

	}
}

﻿using CoinbasePro.Services.Products.Models;
using System;
using System.Windows;
using System.Windows.Data;
using CryptoTracker.Common.ViewModels;
using static System.String;

namespace CryptoTracker.WPF
{
	/// <summary>
	/// Interaction logic for PairSelectView
	/// </summary>
	public partial class PairSelectView : Window
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		public PairSelectView()
		{
			InitializeComponent();
			DataContext = CoinbaseViewModel.Coinbase;
			var view = ( CollectionView )CollectionViewSource.GetDefaultView( CoinbaseViewModel.Coinbase.Products );
			view.Filter = ProductFilter;
		}

		/// <summary>
		/// Event for double clicking list item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ProductsListView_MouseDoubleClick( object sender, System.Windows.Input.MouseButtonEventArgs e )
		{
			if ( !( ProductsListView?.SelectedItem is Product selectedProduct ) ) return;
			CoinbaseViewModel.Coinbase?.AddSubscription( selectedProduct.Id );
			Close();
		}

		/// <summary>
		/// List filter text changed event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ListFilter_TextChanged( object sender, System.Windows.Controls.TextChangedEventArgs e )
		{
			CollectionViewSource.GetDefaultView( CoinbaseViewModel.Coinbase.Products ).Refresh();
		}


		/// <summary>
		/// Method to filter the products list
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private bool ProductFilter( object item )
		{
			if ( IsNullOrEmpty( ListFilter.Text ) )
				return true;
			else
				return ( ( ( Product )item ).DisplayName.IndexOf( ListFilter.Text, StringComparison.OrdinalIgnoreCase ) >= 0 );
		}
	}
}

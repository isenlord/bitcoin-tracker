﻿using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using CryptoTracker.Common.ViewModels;

namespace CryptoTracker.WPF
{
	/// <summary>
	/// Interaction logic for CoinbaseView.xaml
	/// </summary>
	public partial class CoinbaseView : Window
	{
		// Fields
		private PairSelectView pairSelect;

		// Properties
		public CoinbaseViewModel CoinbaseViewModel { get; }

		/// <summary>
		/// Default constructor
		/// </summary>
		public CoinbaseView()
		{
			CoinbaseViewModel = new CoinbaseViewModel( SynchronizationContext.Current );
			InitializeComponent();

			var pairs = CoinbaseViewModel.Coinbase.GetPairs();
			BindingOperations.EnableCollectionSynchronization( pairs, this );

			PairsItemsControl.ItemsSource = pairs;
		}

		/// <summary>
		/// Open the pair selection form
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void AddPairButton_Click( object sender, RoutedEventArgs e )
		{
			if ( pairSelect == null )
			{
				pairSelect = new PairSelectView();
				pairSelect.Closed += PairSelect_Closed;
				pairSelect.Show();
				pairSelect.Top = Top + ( Height - pairSelect.Height ) / 2;
				pairSelect.Left = Left + ( Width - pairSelect.Width ) / 2;
			}

			pairSelect.Activate();
		}

		/// <summary>
		/// What happens when the pair select form is closed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PairSelect_Closed( object sender, System.EventArgs e )
		{
			pairSelect = null;
		}

		private void RemovePairButton_Click( object sender, RoutedEventArgs e )
		{
			if ( !( e.Source is Button button ) ) return;
			if ( button.DataContext is CryptoFiatPairModel cryptoFiatPair )
			{
				CoinbaseViewModel.Coinbase.RemoveSubscription( cryptoFiatPair.Id );
			}
		}
	}
}

﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace CryptoTracker.WPF
{
    public class PercentageColourConverter : IValueConverter
    {
        private readonly Color _negativeColour = Colors.Red;
        private readonly Color _positiveColour = Colors.Green;

        /// <summary>
        /// Convert based on value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( value is string percentage )
            {
                return percentage.Contains( "-" ) ? new SolidColorBrush( _negativeColour ) : new SolidColorBrush( _positiveColour );
            }
            return Binding.DoNothing;
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CryptoTracker.Xam
{
	public class PercentageColourConverter : IValueConverter
	{
		private readonly Color _negativeColour = Color.Red;
		private readonly Color _positiveColour = Color.Green;

		/// <summary>
		/// Convert based on value
		/// </summary>
		/// <param name="value"></param>
		/// <param name="targetType"></param>
		/// <param name="parameter"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			if ( value is string percentage )
			{
				return percentage.Contains( "-" ) ? _negativeColour : _positiveColour;
			}
			return Binding.DoNothing;
		}

		/// <summary>
		/// Does nothing
		/// </summary>
		/// <param name="value"></param>
		/// <param name="targetType"></param>
		/// <param name="parameter"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		}
	}
}

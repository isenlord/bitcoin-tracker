﻿using System;
using System.Threading;
using CryptoTracker.Common.ViewModels;
using Xamarin.Forms;

namespace CryptoTracker.Xam.Views
{
	public partial class MainView : ContentPage
	{
		public CoinbaseViewModel CoinbaseViewModel { get; }

		public MainView()
		{
			CoinbaseViewModel = new CoinbaseViewModel( SynchronizationContext.Current );

			InitializeComponent();

			BindingContext = CoinbaseViewModel.Coinbase.GetPairs();
		}

		private void RemovePairButton_OnClicked( object sender, EventArgs e )
		{
			if ( !( sender is ImageButton button ) ) return;
			if ( !( button.CommandParameter is CryptoFiatPairModel item ) ) return;
			CoinbaseViewModel.Coinbase.RemoveSubscription( item.Id );
		}

		private void AddPairButton_OnClicked( object sender, EventArgs e )
		{
			OpenPairSelectView();
		}

		private async void OpenPairSelectView()
		{
			await Navigation.PushModalAsync( new PairSelectView() );

		}
	}
}

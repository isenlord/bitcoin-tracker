﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using CoinbasePro.Services.Products.Models;
using CryptoTracker.Common.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CryptoTracker.Xam.Views
{
	[XamlCompilation( XamlCompilationOptions.Compile )]
	public partial class PairSelectView : ContentPage
	{
		private ObservableCollection<Product> products;
		public ICommand ProductSelectedCommand => new Command<Product>( SelectProduct );

		public PairSelectView()
		{
			InitializeComponent();

			BindingContext = this;
			products = CoinbaseViewModel.Coinbase.Products;
			PairSelectCollectionView.ItemsSource = products;
		}

		private void FilterProducts( string value )
		{
			var filteredProducts = CoinbaseViewModel.Coinbase.Products
				.Where( x => x.DisplayName.ToLower().Contains( value.ToLower() ) ).ToList();

			products = new ObservableCollection<Product>( filteredProducts );

			PairSelectCollectionView.ItemsSource = products;

		}

		private void SelectProduct( Product product )
		{
			if ( product == null ) return;
			CoinbaseViewModel.Coinbase.AddSubscription( product.Id );
			ReturnHome();
		}

		private async void ReturnHome()
		{
			await Navigation.PopModalAsync();
		}

		private void ProductSearchBar_OnTextChanged( object sender, TextChangedEventArgs e )
		{
			if ( !( sender is SearchBar searchBar ) ) return;
			var text = searchBar.Text ?? "";

			FilterProducts( text );
		}

		private void PairSelectCollectionView_OnSelectionChanged( object sender, SelectionChangedEventArgs e )
		{
			var selection = e.CurrentSelection.FirstOrDefault() as Product;

			SelectProduct( selection );
		}

		private void BackButton_OnClicked( object sender, EventArgs e )
		{
			ReturnHome();
		}
	}
}